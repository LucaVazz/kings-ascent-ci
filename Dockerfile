FROM debian:10.9

EXPOSE 35565
EXPOSE 35565/udp
EXPOSE 35566/udp
EXPOSE 35567/udp
EXPOSE 35568/udp
EXPOSE 35569/udp

WORKDIR /app


# get Godot server
RUN export DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install wget unzip

RUN wget "https://downloads.tuxfamily.org/godotengine/3.3.2/Godot_v3.3.2-stable_linux_server.64.zip"
RUN unzip "Godot_v3.3.2-stable_linux_server.64.zip"

# prepare server
ADD dist .

# run
CMD ["./Godot_v3.3.2-stable_linux_server.64", "--main-pack", "kings_ascent_server.pck", "--verbose"]
